@echo off

@call SET FW_LOG_DIR=..\..\..\logs
@call SET FW_DATA_DIR=..\..\..\data

@call SET SOLR_PATH_CONF=%~dp0..\..\solr
@call SET SOLR_DATA_DIR=%~dp0%FW_DATA_DIR%\solr
@call SET SOLR_LOGS_DIR=%~dp0%FW_LOG_DIR%\solr
@call SET SOLR_BIN_DIR="..\..\..\..\src\solr\target\unpacked\solr-*"
REM loop to resolve path with wildcard
for /f %%a in (%SOLR_BIN_DIR%) do set "SOLR_BIN_DIR=%%~fa\bin"

@if not exist "%SOLR_LOGS_DIR%" call mkdir "%SOLR_LOGS_DIR%"

@echo Starting solr with logs at %SOLR_LOGS_DIR%
call %SOLR_BIN_DIR%\solr.cmd ^
 -Dsolr.lib.dir=%SOLR_PATH_CONF%\lib ^
 -Dresource.dir=%SOLR_PATH_CONF%\..\resources ^
 -p 8082 ^
 -f ^
 -s %SOLR_PATH_CONF% ^
 -t %SOLR_DATA_DIR% ^
 -d %SOLR_BIN_DIR%\..\server ^
 > %SOLR_LOGS_DIR%\solr-stdout.log ^
 2> %SOLR_LOGS_DIR%\solr-stderr.log

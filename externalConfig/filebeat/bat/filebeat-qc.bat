@echo on

@call SET FW_LOG_DIR=..\..\..\logs
@call SET FW_DATA_DIR=..\..\..\data

@call SET FILEBEAT_LOG_DIR=%FW_LOG_DIR%\filebeat
@call SET FILEBEAT_BIN_DIR="..\..\..\..\src\filebeat\target\unpacked\filebeat-*"
REM loop to resolve path with wildcard
for /f %%a in (%FILEBEAT_BIN_DIR%) do set "FILEBEAT_BIN_DIR=%%~fa"

@if not exist "%FILEBEAT_LOG_DIR%" call mkdir "%FILEBEAT_LOG_DIR%"

@echo Starting filebeat-qc with logs at %FILEBEAT_LOG_DIR%
call %FILEBEAT_BIN_DIR%\filebeat.exe ^
 -c %~dp0..\..\filebeat\filebeat-qc.yml ^
 --path.home %~dp0..\..\filebeat ^
 --path.config %~dp0..\..\filebeat ^
 --path.data %~dp0%FW_DATA_DIR%\filebeat-qc ^
 --path.logs %~dp0%FILEBEAT_LOG_DIR% ^
 > %FILEBEAT_LOG_DIR%\filebeat-qc-stdout.log ^
 2> %FILEBEAT_LOG_DIR%\filebeat-qc-stderr.log

@echo on

@call SET FW_LOG_DIR=..\..\..\logs
@call SET FW_DATA_DIR=..\..\..\data

@call SET ES_LOG_DIR=%~dp0%FW_LOG_DIR%\elasticsearch
@call SET KIBANA_LOG_DIR=%~dp0%FW_LOG_DIR%\kibana
@call SET SOLR_LOGS_DIR=%~dp0%FW_LOG_DIR%\solr

@call SET FILEBEAT_LOG_DIR=%FW_LOG_DIR%\filebeat
@call SET FILEBEAT_BIN_DIR="..\..\..\..\src\filebeat\target\unpacked\filebeat-*"
REM loop to resolve path with wildcard
for /f %%a in (%FILEBEAT_BIN_DIR%) do set "FILEBEAT_BIN_DIR=%%~fa"

@if not exist "%FILEBEAT_LOG_DIR%" call mkdir "%FILEBEAT_LOG_DIR%"

@echo Starting filebeat-all with logs at %FILEBEAT_LOG_DIR%
call %FILEBEAT_BIN_DIR%\filebeat.exe ^
 -c %~dp0..\..\filebeat\filebeat-all.yml ^
 --path.home %~dp0..\..\filebeat ^
 --path.config %~dp0..\..\filebeat ^
 --path.data %~dp0%FW_DATA_DIR%\filebeat-all ^
 --path.logs %~dp0%FILEBEAT_LOG_DIR% ^
 > %FILEBEAT_LOG_DIR%\filebeat-all-stdout.log ^
 2> %FILEBEAT_LOG_DIR%\filebeat-all-stderr.log

@echo off

@call SET FW_LOG_DIR=..\..\..\logs
@call SET FW_DATA_DIR=..\..\..\data

@call SET KIBANA_PATH_CONF=%~dp0..\..\kibana
@call SET KIBANA_LOG_DIR=%~dp0%FW_LOG_DIR%\kibana
@call SET KIBANA_BIN_DIR="..\..\..\..\src\kibana\target\unpacked\kibana-*"
REM loop to resolve path with wildcard
for /f %%a in (%KIBANA_BIN_DIR%) do set "KIBANA_BIN_DIR=%%~fa\bin"

@if not exist "%KIBANA_LOG_DIR%" call mkdir "%KIBANA_LOG_DIR%"

@echo Starting kibana with logs at %KIBANA_LOG_DIR%
call %KIBANA_BIN_DIR%\kibana.bat ^
 -c %KIBANA_PATH_CONF%\kibana.yml ^
 -l %KIBANA_LOG_DIR%/kibana.log ^
 > %KIBANA_LOG_DIR%\kibana-stdout.log ^
 2> %KIBANA_LOG_DIR%\kibana-stderr.log

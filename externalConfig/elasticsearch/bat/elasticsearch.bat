@echo off

@call SET FW_LOG_DIR=..\..\..\logs
@call SET FW_DATA_DIR=..\..\..\data

@call SET ES_PATH_CONF=%~dp0..\..\elasticsearch
@call SET ES_DATA_DIR=%~dp0%FW_DATA_DIR%\elasticsearch
@call SET ES_LOG_DIR=%~dp0%FW_LOG_DIR%\elasticsearch
@call SET ES_BIN_DIR="..\..\..\..\src\elasticsearch\target\unpacked\elasticsearch-*"
REM loop to resolve path with wildcard
for /f %%a in (%ES_BIN_DIR%) do set "ES_BIN_DIR=%%~fa\bin"

if not exist %ES_LOG_DIR% call mkdir %ES_LOG_DIR%

@echo Starting elasticsearch with logs at %ES_LOG_DIR%
call %ES_BIN_DIR%/elasticsearch.bat ^
 > %ES_LOG_DIR%\elasticsearch-stdout.log ^
 2> %ES_LOG_DIR%\elasticsearch-stderr.log

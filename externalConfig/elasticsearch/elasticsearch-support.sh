#!/bin/bash


#
# Utility script manage a running elasticsearch instance
# and in particular install required templates & pipelines
# The architecture is like this:
#
#                                   +----------------------------+
#                                   |       Elasticsearch        |
#                                   +----------------------------+
# +---------+       +----------+    | +-----------+    +-------+ |
# | i3 logs |`.  -> | Filebeat | -> | | Pipelines | -> | Index | |
# +---------+ |     +----------+    | +-----------+    +-------+ |
#   `---------+                     |                            |
#                                   +----------------------------+
#
# 1. FileBeat pick up i3 logs
# 2. Logs are sent to elasticsearch pipelines for processing
# 3. Logs are indexed in elasticsearch using the templates mapping
#
#
# Commands:
#
#   putTemplates   - Put all index templates from ./templates/
#                    Supplied are templates for i3 logs and analytics
#
#   putPipelines   - Put all pipelines from ./pipelines/
#                    Supplied are pipelines for i3 logs and analytics
#


#
# Configurables
#

url="http://localhost:9200"
jsonHeader=-H'Content-Type: application/json'
templatesPath="templates"
filebeatPath="../filebeat"
eventsPath="events"

function createIndex {
  echo -e "Creating index ${1}\n"
  curl -sS -XPUT  "${url}/${1}?pretty"
}

function showIndex {
  echo -e "Displaying elastic indices\n"
  curl -sS "${url}/_cat/indices?v"
}

function deleteIndex {
  echo -e "Deleting index $1\n"
  curl -sS -XDELETE "${url}/${1}?pretty"
}

function loglevel {
  echo -e "Changing root loglevel to ${1^^}\n"
  curl -sS -XPUT "${jsonHeader}" "${url}/_cluster/settings?pretty" -d "
  {
    \"transient\" : {
      \"logger._root\" : \"${1^^}\"
    }
 }"
}

function putTemplates {
  echo -e "Putting all templates from ${templatesPath}\n"
  for template in ${@:1}; do
    echo curl -sS -X PUT "${jsonHeader}" "${url}/_template/${template%-template.json}?pretty" -d @${templatesPath}/${template}
    curl -sS -X PUT "${jsonHeader}" "${url}/_template/${template%-template.json}?pretty" -d @${templatesPath}/${template}
    echo
  done
}

function putPipelines {
  echo -e "Putting all elasticsearch pipelines\n"
  for pipeline in ${@:1}; do
    echo curl -sS -X PUT "${jsonHeader}" "${url}/_ingest/pipeline/$(basename ${pipeline%.json})?pretty" -d @${pipeline}
    curl -sS -X PUT "${jsonHeader}" "${url}/_ingest/pipeline/$(basename ${pipeline%.json})?pretty" -d @${pipeline}
    echo
  done
}

function slowlog {
  echo -e "Changing search slog low threshold ${1,,} to $2 (use -1 to disable)\n"
  curl -sS -XPUT "${jsonHeader}" "${url}/_all/_settings?pretty" -d"
  {
      \"index.search.slowlog.threshold.query.${1,,}\": \"$2\"
  }"
}

function usage {
  echo "usage: "
  echo "    $0 <command> [args]"
  echo
  echo "Commands"
  echo "    showIndex                                  - Displays indices"
  echo "    createIndex <index>                        - E.g. createIndex foo"
  echo "    deleteIndex <index>                        - E.g. deleteIndex _all"
  echo "    putTemplates                               - Put all templates in directory ${templatesPath}"
  echo "    putPipelines                               - Put all pipelines in directory ${pipelinesPath}"
  echo "    loglevel <loglevel>                        - Changes the loglevel to one of INFO|WARN|ERROR|DEBUG"
  echo "    slowlog <loglevel> <timeout><s|ms>         - Sets threshold of the slow log, e.g. slowlog warn 200ms"
}

case "$1" in
  createIndex)
    createIndex $2
  ;;

  removeIndex|deleteIndex)
    deleteIndex $2
  ;;

  showIndex|showIndexes|showIndices)
    showIndex
  ;;

  putTemplates)
    putTemplates $(ls ${templatesPath} | grep template)
  ;;

  putPipelines)
    putPipelines $(ls ${filebeatPath}/module/findwise*/*/ingest/*.json)
  ;;

  loglevel)
    loglevel $2
  ;;

  deleteIndex)
    deleteIndex $2
  ;;

  slowlog)
    slowlog $2 $3
  ;;

  help|-help|--help|*)
      usage
  ;;
esac
exit 0

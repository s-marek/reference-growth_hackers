package com.findwise.reference;

import com.findwise.commons.jettywebapp.JettySettings;
import com.findwise.commons.jettywebapp.JettyWebServer;

/**
 * Bootstraps the connect {@code capsule} application.
 *
 * <p>
 * The {@code main} method is invoked by {@code capsule} when running the
 * application standalone using {@code java -jar}
 * </p>
 *
 * @author johan.sjoberg
 */
public class Main {

    /**
     * The project {@code main} class starting up an embedded {@code jetty}
     * intended to run with {@code capsule} as a standalone runnable
     * war/jar-file.
     *
     * @param args command line arguments
     * @throws Exception if an error occurs starting the application
     */
    public static void main(String[] args) throws Exception {
        JettySettings settings = JettySettings
                .create("connectorservice")
                .instanceName("connectorservice")
                .extraClasspath("lib") // permit loading stages from ${fwhome}/connectorservice/lib
                .http(8084)
                .withWebAppInitializerClassName("com.findwise.connect.app.web.config.WebAppInitializer") // required
                .enableGzipResponse()
                .enableAccessLog();
        JettyWebServer.builder(settings).build(args).run();
    }
}

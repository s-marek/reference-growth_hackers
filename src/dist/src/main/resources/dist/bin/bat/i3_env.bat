@echo off
echo Setting i3 system properties

@call SET FW_HOME=../../fwhome
@call SET FW_LOG_DIR=../../logs
@call SET FW_DATA_DIR=../../data

REM Below variables are used for ribbon usage only
call SET FW_I3_BASE_URLS=^
-Dfw.i3.baseUrl.connect="http://localhost:8084" ^
-Dfw.i3.baseUrl.index="http://localhost:8080" ^
-Dfw.i3.baseUrl.process="http://localhost:8081" ^
-Dfw.i3.baseUrl.search="http://localhost:8090" ^
-Dfw.i3.baseUrl.sep="http://localhost:8083" ^
-Dfw.i3.baseUrl.kibana="http://localhost:8085" ^
-Dfw.i3.baseUrl.elastic="http://localhost:9200/" ^
-Dfw.i3.baseUrl.solr="http://localhost:8082/solr"

@echo off
setlocal enableextensions enabledelayedexpansion

call i3_env.bat
call SET INSTANCE_NAME=processing
call SET LOG_DIR="%FW_LOG_DIR%\%INSTANCE_NAME%"
call SET CAPSULE_CACHE_DIR=%~dp0/capsule/%INSTANCE_NAME%
if not exist %LOG_DIR% call mkdir %LOG_DIR%
if not exist "%CAPSULE_CACHE_DIR%" call mkdir "%CAPSULE_CACHE_DIR%"

call SET GC_OPTS=-XX:+UseConcMarkSweepGC ^
 -XX:+UseCMSInitiatingOccupancyOnly ^
 -XX:+ScavengeBeforeFullGC ^
 -XX:+CMSScavengeBeforeRemark ^
 -XX:+CMSParallelRemarkEnabled ^
 -XX:CMSInitiatingOccupancyFraction=70 ^
 -XX:+PrintGCDateStamps ^
 -verbose:gc ^
 -XX:+PrintGCDetails ^
 -Xloggc:%LOG_DIR%/%INSTANCE_NAME%.gc.log ^
 -XX:+UseGCLogFileRotation ^
 -XX:NumberOfGCLogFiles=10 ^
 -XX:GCLogFileSize=100M ^
 -XX:+HeapDumpOnOutOfMemoryError ^
 -XX:HeapDumpPath=%LOG_DIR%/dump.hprof

@call java -Xms256m -Xmx1024m %GC_OPTS% ^
 -Dfw.home=%FW_HOME% ^
 -Dfw.log.dir=%FW_LOG_DIR% ^
 -jar ../processing.war ^
 --port=8081 ^
 --instanceName=%INSTANCE_NAME% ^
 > %LOG_DIR%\jetty-stdout.log ^
 2> %LOG_DIR%\jetty-stderr.log

###############################################################################
# Configuration to build a docker image of the Findwise i3 indexsvc
# service together with its configuration. 
#
# NOTE: The indexsvc requires an externally running mongodb. Use the environment
#       variable MONGO_URI to point to it or configure the URI setting in 
#       fwhome/indexsvc/app.properties
# 
# Build image using docker-compose or manually with
#     docker build -t <name> -f Dockerfile.index .
#
# Run image using docker-compose or manually with
#     docker run --env-file=docker-compose.env.local -p 8080:8080 <name>
#
###############################################################################

FROM openjdk:8-jre

LABEL Maintainer Findwise <product[at]findwise.com> \
    Description="Findwise i3 indexing service" \
    Vendor="Findwise"

WORKDIR /i3
COPY bin/indexsvc.war bin/indexsvc.war
COPY fwhome/indexsvc fwhome/indexsvc/
COPY fwhome/ribbon.html fwhome/ribbon.html

EXPOSE 8080

HEALTHCHECK --interval=30s --timeout=10s --retries=4 \
    CMD curl --fail -L http://localhost:8080/login.html || exit 1

# Directory the i3 artifact is unzipped to when run
ENV CAPSULE_CACHE_DIR=/i3/tmp/

RUN mkdir -p /i3/logs/indexsvc/

ENTRYPOINT java -Xms256M -Xmx2048M -server \
    $CONNECT $INDEX $PROCESS $SEARCH $SEP $KIBANA $ELASTIC $SOLR \
    -Dindexsvc.mongo.uri=$MONGO_URI \
    -Dfw.home=/i3/fwhome \
    -Dfw.data.dir=/i3/data \
    -Dfw.log.dir=/i3/logs \
    -Djava.awt.headless=true \
    -Dcapsule.log=quiet \
    -XX:+UseConcMarkSweepGC \
    -XX:+UseCMSInitiatingOccupancyOnly \
    -XX:+ScavengeBeforeFullGC \
    -XX:+CMSScavengeBeforeRemark \
    -XX:+CMSParallelRemarkEnabled \
    -XX:CMSInitiatingOccupancyFraction=70 \
    -XX:+PrintGCDateStamps -verbose:gc -XX:+PrintGCDetails -Xloggc:"/i3/logs/indexsvc/" \
    -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M \
    -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath="/i3/logs/indexsvc/`date`.hprof" \
    -jar /i3/bin/indexsvc.war

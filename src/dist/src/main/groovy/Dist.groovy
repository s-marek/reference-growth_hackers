import java.nio.file.Paths
import java.nio.file.Files
import java.io.File

/**
 *  Groovy script to create the dist output directory with the
 *  complete deployable i3 system
 *
 *  @author johan.sjoberg
 *  @see http://groovy.github.io/gmaven/groovy-maven-plugin/
 */

// ******************************************************
//   Artifacts to include relative ${basedir}/
// ******************************************************

def artifacts = [
    'src/connectorservice/target/connectorservice.war',
    'src/indexsvc/target/indexsvc.war',
    'src/processing/target/processing.war',
    'src/search/target/search.war',
    'src/sep/target/sep.war'
]

// ******************************************************
//   Prepare dist directory
// ******************************************************

def rootPath = Paths.get(basedir.getParentFile().getParent())

log.info 'Cleaning distribution directory, ' + rootPath.resolve('dist')
rootPath.resolve('dist').toFile().deleteDir()

log.info 'Preparing distribution directory, ' + rootPath.resolve('dist')
rootPath.resolve('dist/fwhome').toFile().mkdirs()
rootPath.resolve('dist/bin/systemd').toFile().mkdirs()

// ******************************************************
//   Copy files to dist directory
// ******************************************************

log.info 'Copying artifacts...'
artifacts.each() { f ->
    def from = rootPath.resolve(f)
    def to = rootPath.resolve('dist/bin').resolve(Paths.get(f).getFileName())
    if (Files.exists(from)) {
        log.info 'Copying: ' + from + ' to ' + to
        Files.copy(from, to)
    }
}

log.info 'Copying scripts...'
ant.copy(todir: rootPath.resolve('dist').toString()) {
    fileset(dir: rootPath.resolve('src/dist/src/main/resources/dist').toString())
}

log.info 'Copying fwhome...'
ant.copy(todir: rootPath.resolve('dist/fwhome').toString()) {
    fileset(dir: rootPath.resolve('fwhome').toString(), excludes: 'pom.xml')
}

log.info 'Copying external configuration files ...'
ant.copy(todir: rootPath.resolve('dist/externalConfig').toString()) {
    fileset(dir: rootPath.resolve('externalConfig').toString(), excludes: 'pom.xml')
}
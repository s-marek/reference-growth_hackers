#!/bin/bash

#
# Compares dependencies against manually collected license files
# to detect if any licenses are missing or redundant
#


#
# Collect all artifact names from all dependencies
#
#      -n                    # don't print by default 
#      -e 's/..........//'   # shave off the first 10 chars 
#      -e 's/:compile$//p'   # only print lines containing compile 
#      -e '/findwise/d'      # drop Findwise dependencies
#      -e 's/^[^:]*://g'     # remove package name 
#      -e 's/:.*//g'         # remove anyhing trailing the artifact name
#      sort, uniq            # only display unique items
readarray -t dependencies <<< $(mvn dependency:list -DexcludeArtifactIds=filebeat-windows-x86_64,filebeat-linux-x86_64 2>/dev/null | sed \
      -n \
      -e 's/..........//' \
      -e 's/:compile$//p' \
      -e 's/:compile //p' \
      -e 's/:runtime$//p' \
      -e 's/:provided$//p' \
      | sed \
      -e '/findwise/d' \
      -e 's/^[^:]*://g' \
      -e 's/:.*//g' \
      | sort \
      | uniq)


#
# Collect all artifact names from license files
#
#    -e 's/\.[^.]*\.license\.txt//'  # strip license part
#    -e 's/\.notice\.txt//'          # strip notice part
#    sort, uniq                      # only display unique items
#
readarray -t licenses <<< $(ls fwhome/thirdparty_licences/java | sed \
    -e 's/\.[^.]*\.license\..*//' \
    -e 's/\.notice\..*//' \
    | sort \
    | uniq)


#
# Compare the differences between collected dependencies and license files
#
#      -1                   # suppress column 1 (lines unique to FILE1)
#      -2                   # suppress column 2 (lines unique to FILE2) 
#      -3                   # suppress column 3 (lines that appear in both files)

echo "Dependencies that are missing license"
echo "======================================"
comm -23 <(printf '%s\n' "${dependencies[@]}") <(printf '%s\n' "${licenses[@]}")
echo


echo
echo "Licenses that are no longer in use"
echo "======================================"
comm -13 <(printf '%s\n' "${dependencies[@]}") <(printf '%s\n' "${licenses[@]}")
echo




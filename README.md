![i3_text_horisontal.png](https://bitbucket.org/repo/6AEL4r/images/1794841094-i3_text_horisontal.png)

# Findwise Reference Architecture

Findwise reference is the preferred approach to setup and deploy Findwise i3. 

[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/reference)](https://build.i3.findwise.com/job/i3%20snapshots/job/reference/) **Reference**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/search)](https://build.i3.findwise.com/job/i3 snapshots/job/search/) **Search**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/processing)](https://build.i3.findwise.com/job/i3 snapshots/job/processing/) **Processing**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/indexsvc3)](https://build.i3.findwise.com/job/i3%20snapshots/job/indexsvc3/) **Indexsvc**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/connector)](https://build.i3.findwise.com/job/i3 snapshots/job/connector/) **Connect**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/commons)](https://build.i3.findwise.com/job/i3%20snapshots/job/commons/) **Commons**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/sep)](https://build.i3.findwise.com/job/i3%20snapshots/job/sep/) **Search editor portal**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/i3-templates)](https://build.i3.findwise.com/job/i3 snapshots/job/i3-templates/) **i3-templates**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/i3-extras)](https://build.i3.findwise.com/job/i3%20snapshots/job/i3-extras/) **i3-extras**  
[![Build Status](https://build.i3.findwise.com/buildStatus/icon?job=i3%20snapshots/commons-js)](https://build.i3.findwise.com/job/i3%20snapshots/job/commons-js/) **Commons-js**  

## How Findwise reference works

The project is designed to be the template of template projects. It uses [capsule](https://github.com/puniverse/capsule) with [jetty](http://www.eclipse.org/jetty/documentation/current/embedding-jetty.html) to create standalone runnable war-files. The project contains the templates of all components of the Findwise architecture which means that cloning this project is sufficient to get started with the Findwise stack. When building from the root all artifacts and configuration are composed into the `dist` directory.

## Getting started

To get started clone the project into new empty git customer repository. You will now have the following directory structure

    :::text
    .
    |-- src
    |   |-- connectorservice   i3 Connector service
    |   |-- indexsvc           i3 Indexing service
    |   |-- processing         i3 Processing service
    |   |-- search             i3 Search service
    |   |-- sep                i3 Search Editor Portal
    |   |-- core               Jetty for embedding into applications
    |   |-- dist               Assembles i3 into the dist directory the build phase
    |   |-- elasticsearch      Elasticsearch search engine
    |   |-- solr               Apache Solr search engine
    |   |-- kibana             Visualize Elasticsearch data
    |   `-- filebeat           Gather logs into kibana
    |-- fwhome                 Common configuration directory
    |-- externalConfig         Configuration directory for external components
    `-- dist                   The runnable i3 distribution; created when src/dist is built


### Build from source

Build with maven 3 from the root. 

```
#!bash

mvn install
```

After a successful build the runnable i3 distribution is now assembled into the `dist` directory.  

### Launch the applications

Prerequisities

* Maven 3.x with `MAVEN_OPTS="-Xmx2048m"`
* Java 1.8
* MongoDB

Start i3 with

```
cd dist/bin && java -jar <artifact>
```

Start the search engine of choice

```
cd src/elasticsearch && mvn exec:exec or mvn antrun:run
cd src/solr && mvn exec:exec or mvn antrun:run
```

On Windows there's known issue with shutdown-hook when using `mvn exec:exec`, that's why there's also delivered way to run with `mvn antrun:run`.

Default all i3 services are protected with form-login with the credentials `admin/superadmin`. This can be configured in respective `app.properties` file. 


# Reference

## System properties

* `fw.home`: Location of runtime configuration. 
* `fw.log.dir`: Location used by logback to store logs. 
* `fw.data.dir`: Location for runtime data files

Applications must respect the structure for the system properties `fw.home`, `fw.log.dir` and `fw.data.dir` by prefixing the directory structure with the application name, e.g., `${fw.home}/indexsvc/`